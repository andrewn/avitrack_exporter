package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var addr = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")

var errNonOKHTTP = errors.New("request failed")

// LogData holds log data.
type LogData struct {
	Data []DataPoint `json:"data"`
}

// DataPoint holds a single data point.
type DataPoint struct {
	Date string `json:"date"`
	Time string `json:"time"`
	Temp string `json:"temp"`
	RHum string `json:"rhum"`
	Solr string `json:"solr"`
	MBar string `json:"mbar"`
	WDir string `json:"wdir"`
	WDeg string `json:"wdeg"`
	WSpd string `json:"wspd"`
	MSpd string `json:"mspd"`
	Rain string `json:"rain"`
	Batt string `json:"batt"`
}

type avitrackCollector struct {
	lastTimestamp *time.Time
	rainCounter   float64
	solrCounter   float64

	tempCounterDesc *prometheus.Desc
	rhumCounterDesc *prometheus.Desc
	solrCounterDesc *prometheus.Desc
	mbarCounterDesc *prometheus.Desc
	wdegCounterDesc *prometheus.Desc
	wspdCounterDesc *prometheus.Desc
	mspdCounterDesc *prometheus.Desc
	rainCounterDesc *prometheus.Desc
	battCounterDesc *prometheus.Desc
}

func (c *avitrackCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.tempCounterDesc
	ch <- c.rhumCounterDesc
	ch <- c.solrCounterDesc
	ch <- c.mbarCounterDesc
	ch <- c.wdegCounterDesc
	ch <- c.wspdCounterDesc
	ch <- c.mspdCounterDesc
	ch <- c.rainCounterDesc
	ch <- c.battCounterDesc
}

func (c *avitrackCollector) dates() (string, string) {
	now := time.Now()
	dateString := now.Format("02/01/2006")

	startTime := now.Add(-(30 * 60 * time.Second))
	startTimeString := startTime.Format("15:04")

	return dateString, startTimeString
}

func (c *avitrackCollector) fetch(dateString string, startTimeString string) (*LogData, error) {
	u, err := url.Parse(`http://www.avitrack.co.za/aws/download_stoney.php`)
	if err != nil {
		return nil, fmt.Errorf("failed to parse url %w", err)
	}

	q := u.Query()
	q.Add("json", "1")
	q.Add("StartDate", dateString)
	q.Add("StartTime", startTimeString)
	u.RawQuery = q.Encode()

	resp, err := http.Get(u.String())
	if err != nil {
		return nil, fmt.Errorf("failed to read server %w", err)
	}

	// Success is indicated with 2xx status codes:
	statusOK := resp.StatusCode >= 200 && resp.StatusCode < 300
	if !statusOK {
		return nil, fmt.Errorf("non-OK HTTP response: %v: %w", resp.StatusCode, errNonOKHTTP)
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %w", err)
	}

	var s LogData

	if err := json.Unmarshal(body, &s); err != nil {
		return nil, fmt.Errorf("failed to unmarshal: %w", err)
	}

	return &s, nil
}

// Collect publishes metrics from avitrack.
func (c *avitrackCollector) Collect(ch chan<- prometheus.Metric) {
	dateString, startTimeString := c.dates()

	s, err := c.fetch(dateString, startTimeString)
	if err != nil {
		log.Printf("fetch failed: %v", err)
		return
	}

	if len(s.Data) < 1 {
		log.Printf("no data")
		return
	}

	item := s.Data[len(s.Data)-1]

	ts, err := time.Parse("02/01/2006T15:04", dateString+"T"+item.Time)
	if err != nil {
		log.Printf("failed to parse time %v", err)
		return
	}

	isNewReading := true
	if c.lastTimestamp != nil && *c.lastTimestamp == ts {
		isNewReading = false
	}

	c.lastTimestamp = &ts

	c.updateTemp(ch, item, ts, isNewReading)
	c.updateRhum(ch, item, ts, isNewReading)
	c.updateSolr(ch, item, ts, isNewReading)
	c.updateMbar(ch, item, ts, isNewReading)
	c.updateWdeg(ch, item, ts, isNewReading)
	c.updateWspd(ch, item, ts, isNewReading)
	c.updateMspd(ch, item, ts, isNewReading)
	c.updateRain(ch, item, ts, isNewReading)
	c.updateBatt(ch, item, ts, isNewReading)
}

func (c *avitrackCollector) updateTemp(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	// temp
	temp, err := strconv.ParseFloat(item.Temp, 64)
	if err != nil {
		log.Printf("failed to read temperature %v", err)
		return
	}

	tempMetric := prometheus.MustNewConstMetric(
		c.tempCounterDesc,
		prometheus.GaugeValue,
		temp,
	)

	ch <- tempMetric
}

func (c *avitrackCollector) updateRhum(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	rhum, err := strconv.ParseFloat(item.RHum, 64)
	if err != nil {
		log.Printf("failed to read rhum %v", err)
		return
	}

	rhum = rhum / 100

	rhumMetric := prometheus.MustNewConstMetric(
		c.rhumCounterDesc,
		prometheus.GaugeValue,
		rhum,
	)
	ch <- rhumMetric
}

func (c *avitrackCollector) updateSolr(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	solr, err := strconv.ParseFloat(item.Solr, 64)
	if err != nil {
		log.Printf("failed to read solr %v", err)
		return
	}

	if solr > 0 && isNewReading {
		c.solrCounter = c.solrCounter + solr
	}

	solrMetric := prometheus.MustNewConstMetric(
		c.solrCounterDesc,
		prometheus.CounterValue,
		c.solrCounter,
	)

	ch <- solrMetric
}

func (c *avitrackCollector) updateMbar(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	mbar, err := strconv.ParseFloat(item.MBar, 64)
	if err != nil {
		log.Printf("failed to read mbar %v", err)
		return
	}

	mbarMetric := prometheus.MustNewConstMetric(
		c.mbarCounterDesc,
		prometheus.GaugeValue,
		mbar,
	)

	ch <- mbarMetric
}

func (c *avitrackCollector) updateWdeg(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	wdeg, err := strconv.ParseFloat(item.WDeg, 64)
	if err != nil {
		log.Printf("failed to read wdeg %v", err)
		return
	}

	wdegMetric := prometheus.MustNewConstMetric(
		c.wdegCounterDesc,
		prometheus.GaugeValue,
		wdeg,
	)

	ch <- wdegMetric
}

func (c *avitrackCollector) updateWspd(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	wspd, err := strconv.ParseFloat(item.WSpd, 64)
	if err != nil {
		log.Printf("failed to read wspd %v", err)
		return
	}

	// Convert from m/s into km/h
	wspd = wspd * 3.6

	wspdMetric := prometheus.MustNewConstMetric(
		c.wspdCounterDesc,
		prometheus.GaugeValue,
		wspd,
	)

	ch <- wspdMetric
}

func (c *avitrackCollector) updateMspd(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	mspd, err := strconv.ParseFloat(item.MSpd, 64)
	if err != nil {
		log.Printf("failed to read mspd %v", err)
		return
	}

	// Convert from m/s into km/h
	mspd = mspd * 3.6

	mspdMetric := prometheus.MustNewConstMetric(
		c.mspdCounterDesc,
		prometheus.GaugeValue,
		mspd,
	)

	ch <- mspdMetric
}

func (c *avitrackCollector) updateRain(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	rain, err := strconv.ParseFloat(item.Rain, 64)
	if err != nil {
		log.Printf("failed to read rain %v", err)
		return
	}

	if rain > 0 && isNewReading {
		c.rainCounter = c.rainCounter + rain
	}

	rainMetric := prometheus.MustNewConstMetric(
		c.rainCounterDesc,
		prometheus.CounterValue,
		c.rainCounter,
	)

	ch <- rainMetric
}

func (c *avitrackCollector) updateBatt(ch chan<- prometheus.Metric, item DataPoint, ts time.Time, isNewReading bool) {
	batt, err := strconv.ParseFloat(item.Batt, 64)
	if err != nil {
		log.Printf("failed to read batt %v", err)
		return
	}

	battMetric := prometheus.MustNewConstMetric(
		c.battCounterDesc,
		prometheus.GaugeValue,
		batt,
	)

	ch <- battMetric
}

func newAvitrackCollector() *avitrackCollector {
	return &avitrackCollector{
		lastTimestamp: nil,
		solrCounter:   0,
		rainCounter:   0,

		tempCounterDesc: prometheus.NewDesc("avitrack_temperature_celsius", "Temperature in celsius", nil, nil),
		rhumCounterDesc: prometheus.NewDesc("avitrack_relative_humidity", "Relative humidity as a fraction between 0 and 1", nil, nil),
		solrCounterDesc: prometheus.NewDesc("avitrack_solar_yield_mj_per_m2", "Solar yield in watts/m2", nil, nil),
		mbarCounterDesc: prometheus.NewDesc("avitrack_atmospheric_pressure_mbar", "Atmospheric pressure in millibar", nil, nil),
		wdegCounterDesc: prometheus.NewDesc("avitrack_wind_direction_deg", "Wind direction in degrees", nil, nil),
		wspdCounterDesc: prometheus.NewDesc("avitrack_avg_wind_speed_kmh", "Average Wind speed in km/h", nil, nil),
		mspdCounterDesc: prometheus.NewDesc("avitrack_max_wind_speed_kmh", "Max wind speed in km/h", nil, nil),
		rainCounterDesc: prometheus.NewDesc("avitrack_rain_mm", "Rain in millimetres", nil, nil),
		battCounterDesc: prometheus.NewDesc("avitrack_battery_volts", "Station battery in volts", nil, nil),
	}
}

func main() {
	prometheus.MustRegister(newAvitrackCollector())

	flag.Parse()
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(*addr, nil))
}
