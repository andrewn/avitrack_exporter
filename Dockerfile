FROM alpine:3.16
ENTRYPOINT ["/usr/bin/avitrack_exporter"]
COPY avitrack_exporter /usr/bin
